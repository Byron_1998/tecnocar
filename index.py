from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/ingresar')
def ingresar():
    return render_template('ingresar.html')


@app.route('/modificar')
def modificar():
    return render_template('modificar.html')


@app.route('/consultar')
def consultar():
    return render_template('consultar.html')


@app.route('/eliminar')
def eliminar():
    return render_template('eliminar.html')


@app.route('/registro')
def registro():
    return render_template('registro.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/about')
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(debug=True)

